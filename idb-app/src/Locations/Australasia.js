import React from 'react';
import './Australasia.css'; 
import 'bootstrap/dist/css/bootstrap.min.css';
import AUA from './Maps/Australasia.png'

class Australasia extends React.Component {
  render() {
    return (
      <div className="Australasia"> 
	<h1>Australasia</h1>
       <img src={AUA} class="map-img"/>	
	    <div class="container">
  <div class="row">
    <div class="col">
	<h6 class="card-subtitle mb-2 text-muted">Australia and New Zealand</h6>
      <p class="card-text">Population: 29.4 Million <br/>Mortality Rate: 7.3(/1000)<br/>Deaths from preventable disease: 3.40%</p>
    </div>
    <div class="col">
      Reported Disease cases per year: <li>Cholera: 2</li>
	    <li>Malaria: 0</li>
	    <li>Measles: 133</li>
	    <li>Tuberculosis: 2,050</li>
	    <li>Yellow Fever: 0</li>
    </div>
  </div>
</div>
      </div>
    );
  }
}

export default Australasia;
