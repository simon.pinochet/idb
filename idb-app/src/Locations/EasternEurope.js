import React from 'react';
import './EasternEurope.css'; 
import 'bootstrap/dist/css/bootstrap.min.css';
import EE from './Maps/EasternEurope.png';

class EasternEurope extends React.Component {
  render() {
    return (
      <div className="EasternEurope"> 
	<h1>Eastern Europe</h1>
       <img src={EE} class="map-img"/>	
	    <div class="container">
  <div class="row">
    <div class="col">
	<h6 class="card-subtitle mb-2 text-muted">Belarus, Estonia, Latvia, Lithuania, Moldova, Russia, and Ukraine</h6>
      <p class="card-text">Population: 208.5 Million <br/>Mortality Rate: 7.7(/1000)<br/>Deaths from preventable diseases: 37.4%</p>
    </div>
    <div class="col">
      Reported Disease cases per year: <li>Cholera: 39</li>
	    <li>Malaria: 0</li>
	    <li>Measles: 55,824</li>
	    <li>Tuberculosis: 128,820</li>
	    <li>Yellow Fever: 0</li>
    </div>
  </div>
</div>
      </div>
    );
  }
}

export default EasternEurope;
