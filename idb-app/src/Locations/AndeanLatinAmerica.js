import React from 'react';
import './AndeanLatinAmerica.css'; 
import 'bootstrap/dist/css/bootstrap.min.css';
import ALA from './Maps/AndeanLatinAmerica.png'

class AndeanLatinAmerica extends React.Component {
  render() {
    return (
      <div className="AndeanLatinAmerica"> 
	<h1>Andean Latin America: Bolivia, Ecuador, and Peru</h1>
       <img src={ALA} class="map-img"/>	
	    <div class="container">
  <div class="row">
    <div class="col">
	<h6 class="card-subtitle mb-2 text-muted">Bolivia, Ecuador, and Peru</h6>
      <p class="card-text">Population: 29.4 Million <br/>Mortality Rate: 7.3(/1000)<br/>Deaths from preventable disease: 3.40%</p>
    </div>
    <div class="col">
      Reported Disease cases per year: <li>Cholera: 487</li>
	    <li>Malaria: 77,257</li>
	    <li>Measles: 61</li>
	    <li>Tuberculosis: 56,200</li>
	    <li>Yellow Fever: 11</li>
    </div>
  </div>
</div>
      </div>
    );
  }
}

export default AndeanLatinAmerica;
