import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import Home from './Home';
import Causes from './Causes';
import Locations from './Locations';
import About from './About';
import Diseases from './Diseases'
import Vibrio from './Causes/Vibrio';
import FruitBats from './Causes/FruitBats';
import Aedes from './Causes/Aedes';
import AndeanLatinAmerica from './Locations/AndeanLatinAmerica';
import Australasia from './Locations/Australasia';
import EasternEurope from './Locations/EasternEurope';
import DiseasesPage from './Diseases/DiseasesPage'



function App() {
  return (
     <div>
        <Navbar bg="dark" variant="dark">
	  <Navbar.Brand href="/">THO</Navbar.Brand>
      <Nav className="mr-auto">
      <Nav.Link href="/Causes">Causes</Nav.Link>
      <Nav.Link href="/Locations">Locations</Nav.Link>
	  <Nav.Link href="/About">About</Nav.Link>
      <Nav.Link href="/Diseases">Diseases</Nav.Link>
	</Nav>
	</Navbar>
       <BrowserRouter>
	<Switch>
          <Route exact path="/" component={Home} />
	  <Route exact path="/Causes" component={Causes} />
	  <Route exact path="/Locations" component={Locations} />
	  <Route exact path="/About" component={About} />
      <Route exact path="/Diseases" component={Diseases} />
	  <Route exact path="/Causes/Vibrio" component={Vibrio} />
	  <Route exact path="/Causes/FruitBats" component={FruitBats} />
	  <Route exact path="/Causes/Aedes" component={Aedes} />
	  <Route exact path="/Locations/AndeanLatinAmerica" component={AndeanLatinAmerica} />
	  <Route exact path="/Locations/Australasia" component={Australasia} />
	  <Route exact path="/Locations/EasternEurope" component={EasternEurope} />
      <Route exact path="/Diseases/:disease" component={DiseasesPage} />
	</Switch>
        </BrowserRouter>
     </div>
  );
}

export default App;
