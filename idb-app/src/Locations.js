import React from 'react';
import './Locations.css'; 
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import ALA from './Locations/Maps/AndeanLatinAmericaSquare.png'
import AUA from './Locations/Maps/AustralasiaSquare.png'
import EE from './Locations/Maps/EasternEuropeSquare.png'

class Locations extends React.Component {
  render() {
    return (
      <div className="Locations"> 
	    <h1><br/>Locations<br/><br/></h1>
<div class="card-deck">
  <div class="card">
    <img class="card-img-top" src={ALA} alt="Card image cap"/>
    <div class="card-body">
      <h5 class="card-title"><a href="/Locations/AndeanLatinAmerica">Andean Latin America</a></h5>
	<h6 class="card-subtitle mb-2 text-muted">Bolivia, Ecuador, and Peru</h6>
      <p class="card-text">Population: 29.4 Million <br/>Mortality Rate: 7.3(/1000)<br/>Deaths from preventable disease: 3.40%</p>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src={AUA} alt="Card image cap"/>
    <div class="card-body">
      <h5 class="card-title"><a href="/Locations/Australasia">Australasia</a></h5>
	<h6 class="card-subtitle mb-2 text-muted">Australia and New Zealand</h6>
      <p class="card-text">Population: 29.4 Million <br/>Mortality Rate: 7.3(/1000)<br/>Deaths from preventable disease: 3.40%</p>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src={EE} alt="Card image cap"/>
    <div class="card-body">
      <h5 class="card-title"><a href="/Locations/EasternEurope">Eastern Europe</a></h5>
	<h6 class="card-subtitle mb-2 text-muted">Belarus, Estonia, Latvia, Lithuania, Moldova, Russia, and Ukraine</h6>
      <p class="card-text">Population: 208.5 Million <br/>Mortality Rate: 7.7(/1000)<br/>Deaths from preventable diseases: 37.4%</p>
    </div>
    </div>
  </div>
</div>
	  
	
    );
  }
}

export default Locations;
