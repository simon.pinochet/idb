import React from 'react';
import './Causes.css'; 
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import vibrio from './Causes/images/vibrio.jpg';
import fruitbats from './Causes/images/fruitbats.jpg';
import aedes from './Causes/images/aedes.jpg';


class Causes extends React.Component {
  render() {
    return (
      <div className="Causes"> 
	    <h1>List of Causes of Diseases</h1>
	      <div className="row">
		    <div class="col-sm-4">
	          <h2><Link to= "/Causes/Vibrio">Vibrio cholerae</Link></h2>
	            <img src={vibrio} class="imgcauses"/> 
		    </div>
		    <div class="col-sm-4">	
		  	  <h2><Link to= "/Causes/FruitBats">Fruit Bats</Link></h2>
		  	  <img src={fruitbats} class="imgcauses"/>
		  	</div>
	        <div class="col-sm-4">
	  	      <h2><Link to= "/Causes/Aedes">Aedes/Haemogogus mosquitoes</Link></h2>
	  	      <img src={aedes} class="imgcauses"/>
	  	      </div>
	        </div>

	  </div>
	
    );
  }
}

export default Causes;
