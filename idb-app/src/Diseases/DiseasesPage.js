import React from 'react';
import './stylesheet.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import HIVAIDS from './HIVAIDS'
import Malaria from './Maraira'
import Tuberculosis from './Tuberculosis'
import {BrowserRouter, Route} from 'react-router-dom';


class DiseasesPage extends React.Component {
  render() {
    return (
      <div className="LocationPage">
	<h1>Diseases Page: {this.props.match.params.disease}</h1>
	<BrowserRouter>
	<Route exact path="/Diseases/HIVAIDS" component={HIVAIDS} />
	<Route exact path="/Diseases/Malaria" component={Malaria} />
	<Route exact path="/Diseases/Tuberculosis" component={Tuberculosis} />
	    </BrowserRouter></div>
    );
  }
}
export default DiseasesPage;
