import React from 'react';
import './stylesheet.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class Tuberculosis extends React.Component {
  render() {
    return (
      <div className="title">
      <ul>
         <li>Mild - a low-grade fever, night sweats, weakness or tiredness, and weight loss.</li>
         <li>1.3 million deaths / year</li>
      </ul>
      </div>
    );
  }
}

export default Tuberculosis;
