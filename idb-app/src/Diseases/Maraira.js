import React from 'react';
import './stylesheet.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class Malaria extends React.Component {
  render() {
    return (
      <div className="title">
          <ul>
             <li>Severe - serious organ failures or abnormalities in the patient’s blood or metabolism</li>
             <li>445,000 death / year</li>
          </ul>
      </div>
    );
  }
}

export default Malaria;
