import React from 'react';
import './Diseases.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';


class Diseases extends React.Component {
  render() {
    return (
      <div className="list">
	       <p>List of Diseases</p>
          <li> <Link to= "/Diseases/HIVAIDS">HIV/AIDS</Link></li>
	         <li><Link to= "/Diseases/Malaria">Malaria</Link></li>
	  <li><Link to= "/Diseases/Tuberculosis">Tuberculosis</Link></li>

</div>

    );
  }
}

export default Diseases;
