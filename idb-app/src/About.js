import React  from 'react';
import './About.css'; 
import chase from './img/chase.jpg';
import ryan from './img/rsz_ryan.png';
import megan from './img/megan.jpg';
import tae from './img/tae.jpg';
import simon from './img/simon.jpg';
import 'bootstrap/dist/css/bootstrap.min.css';


//Class of Cumulative
class Cumulative extends React.Component {
	

	render () {
		return (
		  <div class='column'>
			<div class='card'>
			  <div class="container">
				<h2 class='card-title'>{this.props.name}</h2>
				<p><large>{this.props.bio}</large></p>
				
				<div class='card-text'>
				  Number of Issues: {this.props.issues}
				</div>
				<p>
				</p>
	
				<div class='card-text'>
				  Number of Commits: {this.props.commits}
				</div>
				<p>
				</p>
	
				<div class='card-text'>
				  Number of Tests: {this.props.tests}
				</div>
				<p>
				</p>
			  </div>
	
			</div>
		  </div>
		)
	  }

}

//Class of person
class Developer extends React.Component {

  
	render () {
	  return (
		<div class='column'>
		  <div class='card'>
			<div class="container">
				<div class="row">
					<div class = "col-md-6">
						<h2 class='card-title'>{this.props.name}</h2>					
						<div class='card-text'>
							Number of Issues: {this.props.issues}
						</div>
						<p>
						</p>
			
						<div class='card-text'>
							Number of Commits: {this.props.commits}
						</div>
						<p>
						</p>
			
						<div class='card-text'>
							Number of Tests: {this.props.tests}
						</div>
						<p>
						</p>
						<p> 
							Tasks: {this.props.task}
						</p>
					</div>
					<div class = "col-md-6">
						<img
							src = {this.props.img}
							 height='225'
							 width='225'
							 class="imgabout"
							 alt = 'Developer Headshots'

						/>
					</div>
				</div>
			</div>
  
		  </div>
		</div>
	  )
	}
  }
  
  // create about to store each person's data 
  class About extends React.Component {
	constructor (props) {
	  super(props)
  
	  this.state = {}
	  this.state.totals ={
		'Total Gitlab Data': {
			name: 'Cumulative',
			commits: 0,
			issues: 0,
			tests: 0
		}
	  }
	  this.state.developerData = {  
		
		'Ryan Gutierrez': {
		  name: 'Ryan Gutierrez',
		  img: ryan,
		  gitlabID: ['Ryan Gutierrez'],
		  task: 'Setting up AWS, Creating Causes pages, Team Lead',
		  commits: 0,
		  issues: 0,
		  tests: 0
		},
		'Chase Heath': {
		  name: 'Chase Heath',
		  img: chase,
		  gitlabID: ['Chase Heath'],
		  task: 'Setting up the About page',
		  commits: 0,
		  issues: 0,
		  tests: 0
		},
		'Simon Pinochet': {
		  name: 'Simon Pinochet',
		  img: simon,
		  gitlabID: ['Simon Pinochet Concha'],
		  task: 'API',
		  commits: 0,
		  issues: 0,
		  tests: 0
		},
		'Megan Mealey': {
		  name: 'Megan Mealey',
		  img: megan,
		  gitlabID: ['Megan Mealey'],
		  task: 'Creating Location pages',
		  commits: 0,
		  issues: 0,
		  tests: 0
		},
		'Tae Kim': {
		  name: 'Tae Kim',
		  img: tae,
		  gitlabID: ['Taehyoung Kim'],
		  task: 'Creating Disease pages',
		  commits: 0,
		  issues: 0,
		  tests: 0
		}
	  }
	}
  
  
  // Fetch data from gitlab api
  // calculate the total number of commits/ issues for each contributor
  componentDidMount (){
	document.title = "About"
	let mystate = this.state;
	fetch(
	  'https://gitlab.com/api/v4/projects/14551859/repository/commits'
	)
	  .then(response => response.json())
	  .then(data => {
		// process the data
		console.log(data)
		for (var i in data) {
		  let commit_data = data[i]
		  for (var dev in this.state.developerData) {
			if (mystate.developerData[dev].gitlabID.includes(commit_data.author_name)) {
			  mystate.developerData[dev].commits += 1;
			}
		  }
		  mystate.totals['Total Gitlab Data'].commits += 1;
		}
		this.setState({})
	  })
	  .catch(e => {
		console.log("Error")
		console.log(e)
	  })
  
  
	fetch('https://gitlab.com/api/v4/projects/14551859/issues')
	  .then(issues => issues.json())
	  .then(issues => {
		  console.log(issues)
		for (var j in issues) {
		  let data = issues[j]
		  for (var dev in this.state.developerData) {
			if (mystate.developerData[dev].gitlabID.includes(data.author.name)) {
			  mystate.developerData[dev].issues += 1;
			}
		  }
		  mystate.totals['Total Gitlab Data'].issues += 1;
		}
		this.setState({})
	  })
	  .catch(e => {
		console.log("Error")
		console.log(e)
	  })
	}
  
	// Rendering all of the updated commits and issues for each developer
	render () {
	  let cards = []
	  let totalCard = []
		for (let dev in this.state.developerData) {
			cards.push(
				<div id={dev} class='col-md-6 mt-4'>
					<div class='card border-0 shadow'>
						<Developer {...this.state.developerData[dev]} />
					</div>
				</div>
			)
	  }
	  totalCard.push(
		<div id={'Total Gitlab Data'} class='col-md-6 mt-4'>
			<div class='card border-0 shadow'>
				<Cumulative {...this.state.totals['Total Gitlab Data']} />
			</div>
		</div>
	)
	  //Display Results
	  return (
		<div class='main'>
			<h1 class="mt-3">About</h1>
		  
		  {/* display group developers */}
		  <div class='container'>


			<div class='row justify-content-center'>
				{cards}
			</div>
			<div class='row justify-content-center mt-4'>
				{totalCard}
			</div>

		  </div>
		  <div class='row' >
			<div class='card-body'>
				<h1 class='card-title'>{this.props.name}</h1>
				<div class='card-text'>{this.props.role}</div>
			</div>
		  </div>

		</div>
	  )
	}
  
  }
  
  export default About
  
