import React from 'react';
import './Aedes.css'; 
import 'bootstrap/dist/css/bootstrap.min.css';

class Aedes extends React.Component {
  render() {
    return (
      <div className="Aedes"> 
	<h1>Aedes and Haemogogus mosquitoes</h1>
	<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Attribute</th>
      <th scope="col">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">About</th>
      <td>Aedes and Haemogogus mosquitoes</td>
    </tr>
    <tr>
      <th scope="row">Afflicted Disease</th>
      <td>Yellow Fever (Flavivirus)</td>
    </tr>
    <tr>
      <th scope="row">Locations/Region</th>
      <td>South America, West and Central Africa</td>
    </tr>
    <tr>
      <th scope="row">Incubation Period</th>
      <td>3 to 6 days</td>
    </tr>
    <tr>
      <th scope="row">Potency</th>
      <td>A small proportion develop severe symptoms and approximately half die within 7 to 10 days</td>
    </tr>
  </tbody>
</table>
      </div>
    );
  }
}

export default Aedes;
