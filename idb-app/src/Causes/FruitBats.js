import React from 'react';
import './FruitBats.css'; 
import 'bootstrap/dist/css/bootstrap.min.css';

class FruitBats extends React.Component {
  render() {
    return (
      <div className="FruitBats"> 
	<h1>Fruit Bats</h1>
	<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Attribute</th>
      <th scope="col">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">About</th>
      <td>Fruit bats of the Pteropodidae family</td>
    </tr>
    <tr>
      <th scope="row">Afflicted Disease</th>
      <td>Ebola virus disease (EVD)</td>
    </tr>
    <tr>
      <th scope="row">Locations/Region</th>
      <td>West/Central Africa: 	Sierra Leone, Liberia, Guinea, Democratic Republic of the Congo</td>
    </tr>
    <tr>
      <th scope="row">Incubation Period</th>
      <td>2 to 21 days</td>
    </tr>
    <tr>
      <th scope="row">Potency</th>
      <td>Average fatality rate: 50%<br></br>
      Case fatality rates: between 25% and 90%</td>
    </tr>
  </tbody>
</table>
      </div>
    );
  }
}

export default FruitBats;
