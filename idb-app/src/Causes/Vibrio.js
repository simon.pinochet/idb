import React from 'react';
import './Vibrio.css'; 
import 'bootstrap/dist/css/bootstrap.min.css';

class Vibrio extends React.Component {
  render() {
    return (
      <div className="Vibrio"> 
	<h1>Vibrio cholerae</h1>
	<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Attribute</th>
      <th scope="col">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">About</th>
      <td>Bacterium</td>
    </tr>
    <tr>
      <th scope="row">Afflicted Disease</th>
      <td>Cholera</td>
    </tr>
    <tr>
      <th scope="row">Locations/Region</th>
      <td>Africa: Algeria, Angola, Burundi, Cameroon, Democratic Republic of the Congo,
      Ethiopia, Kenya, Malawi, Mozambique, Niger, Nigeria, Somalia, Tanzania, 
      Uganda, Zambia, Zimbabwe<br></br>
	  Asia: Bangladesh, India, Yemen<br></br>
	  Americas: Dominican Republic, Haiti</td>
    </tr>
    <tr>
      <th scope="row">Incubation Period</th>
      <td>12 hours and 5 days after ingesting contaminated food or water</td>
    </tr>
    <tr>
      <th scope="row">Potency</th>
      <td>1.3 million to 4.0 million cases each year<br></br>
      21,000 to 143,000 deaths worldwide</td>
    </tr>
  </tbody>
</table>
      </div>
    );
  }
}

export default Vibrio;
